Pi Hackers Project Cards
========================

An experiment in graphics design using online tools. 

Project cards produced using www.sumopaint.com


Overview
--------

I thought it would be neat to have a card deck with Raspberry Pi project ideas on them. 

Just shuffle the deck, pick a card, and start working on your next project. 

